import serial

class my_serial():

    def __init__(self,com):

        self.com = com

    #返回串口对象
    def gene_serial(self):

        ser = serial.Serial(port=self.com, baudrate=115200, timeout=2)

        return ser
