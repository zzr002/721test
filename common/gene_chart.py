import re

import matplotlib.pyplot as plt


def Gene_diagram(x, y, filename):
    plt.title('ping Analysis')
    plt.plot(x, y, color='green', label='rate')
    plt.legend()  # 显示图例
    plt.xlabel('')
    plt.ylabel('ms')

    plt.savefig("./ppp_diagiam/"+filename+".png")
    # plt.show()
