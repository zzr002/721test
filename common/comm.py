import re
import subprocess


class Gene_List():

    def __init__(self, filename):
        self.filename = filename
        self.x = []
        self.y = []

    def read_file(self):
        # print(self.filename)
        with open("ppp_log/"+self.filename + ".txt","r+") as f:
            self.content = f.read()

        self.yyy = re.findall(r"time=(\w+.\w+|\w) ms", self.content)

    def set_y(self):
        for i in range(len(self.yyy)):
            self.y.append(self.yyy.pop())
        self.y.reverse()
        self.y = [float(j) for j in self.y]
        return self.y

    def set_x(self):

        for i in range(self.y.__len__()):
            self.x.append(i+1)
        return self.x

    def run(self):
        self.read_file()
        y = self.set_y()
        x = self.set_x()
        return x,y


