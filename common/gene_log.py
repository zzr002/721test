import datetime


def Get_Time():
    return datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

now_time = Get_Time()


def write_log(at_cmd, at_rep, at_time, rep_time, log_name):
    file_name = "log/" + now_time + log_name
    with open(file_name, "a+") as f:
        f.write(at_time + " >> \r\n" + at_cmd + "\r\n")
        f.write(rep_time+ " << \r\n" +at_rep)

