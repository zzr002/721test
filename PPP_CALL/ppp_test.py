import os
import time
import subprocess



def pre_exec():
    os.setpgid(0, 0)

class Ppp():


    def __init__(self, port):
        self.ppp_call_times = -1
        self.port = port

    def start_ppp(self):
        print("正在进行PPP拨号")
        ppp_pipe = subprocess.Popen(["./../cfg/ppp/air-ppp-on", "--device=%s" % self.port], bufsize=1, preexec_fn=pre_exec,
                                    shell=False, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(10)
        self.ppp_call_times += 1
        cons = "当前ppp断链重拨次数为%d\r\n" % self.ppp_call_times
        print(cons)
        with open("ppp_log/recall.txt", "w") as f:
            f.write(cons)


    def check_status(self):

        statu = subprocess.Popen("./../cfg/ppp/air-ppp-status", shell=False, bufsize=1, stdout=subprocess.PIPE)
        time.sleep(1)
        return statu

    def stop_ppp(self):

        print("正在断开PPP拨号")
        subprocess.Popen("./../cfg/ppp/air-ppp-off", shell=False, bufsize=1, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.check_status()
        time.sleep(1)
