import os
import sys
print("请关闭其他网络，防止影响测试结果！！！")
pwd = os.getcwd()
f_pwd = os.path.dirname(pwd)
sys.path.insert(0, f_pwd)

import multiprocessing
from common.gene_chart import Gene_diagram
from common.auto import Auto
from common.trace import Gene_Trace
from ppp_ping_test import Ping_Test
from common.comm import Gene_List

diag = Auto("diag").reco_com()
gt = Gene_Trace(diag, "ppp_trace/")
p1 = multiprocessing.Process(target=gt.start_trace)
p1.start()

pt = Ping_Test()
file_name = pt.txt_name
try:
    pt.ping()
except Exception as e:
    raise e
finally:
    gl = Gene_List(file_name)
    x, y = gl.run()
    Gene_diagram(x, y, file_name)
    gt.stop_trace()