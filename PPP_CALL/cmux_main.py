import os
import sys
print("请关闭其他网络，防止影响测试结果！！！")
pwd = os.getcwd()
f_pwd = os.path.dirname(pwd)
sys.path.insert(0, f_pwd)

import multiprocessing
from cmux_test import Cmux_Test
from common.comm import Gene_List
from common.gene_chart import Gene_diagram
from common.auto import Auto
from common.trace import Gene_Trace
from cmux_ppp_ping_test import Ping_Test

diag = Auto("diag").reco_com()
gt = Gene_Trace(diag, "ppp_trace/")
p1 = multiprocessing.Process(target=gt.start_trace)
p1.start()

ct = Cmux_Test("/dev/ttyUSB0")
ct.start_cmux()
pt = Ping_Test()
try:
    pt.ping()
except Exception as e:
    raise e
finally:
    file_name = pt.txt_name
    # ct.stop_cmux()
    gl = Gene_List(file_name)
    x, y = gl.run()
    Gene_diagram(x, y, file_name)
    ct.stop_cmux()
    gt.stop_trace()