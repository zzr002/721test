import subprocess
import time
import os

class Cmux_Test():

    def __init__(self, port):
        self.port = port

    def start_cmux(self):
        print("正在打开cmux功能,请稍等")
        cmux_shell_obj = subprocess.Popen(["sudo", "./../bin/cmux", self.port], shell=False, bufsize=1
                                          ,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        cmux_shell_obj.communicate(1000)
        time.sleep(5)

    def stop_cmux(self):
        print("正在关闭cmxu，请稍后重启模块")
        print("对不起，发现有时并不能关闭cmux功能，而该死的程序猿还不知道为什么。如果cmux功能没关上，请试试执行[sudo ./../bin/off-mux]以关闭cmux功能，并重启模块！！")
        off_cmux = subprocess.Popen(["./../bin/off-mux"], shell=False, bufsize=1,stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        off_cmux.communicate(1000)
        time.sleep(5)

#
# ct = Cmux_Test("/dev/ttyUSB0")
#
# # ct.start_cmux()
# ct.stop_cmux()
