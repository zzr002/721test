from src.auto_com import Auto
from src.My_Serial import My_Serial
from trace.gene_trace import Gene_Trace
import multiprocessing

at = Auto("at").reco_com()
diag = Auto("diag").reco_com()

#生成trace
gt = Gene_Trace(diag)
p1 = multiprocessing.Process(target=gt.start_trace)
p1.start()


#开启AT测试
ser = My_Serial(at)
try:
    ser.run()
except KeyboardInterrupt as e:
    print("自动化测试脚本工具关闭！！！")
finally:
    gt.stop_trace()
