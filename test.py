def toHex(num):
    """
    :type num: int
    :rtype: str
    """
    chaDic = {10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f'}
    if num >= 0:
        hexStr = ""
        while num >= 16:
            rest = num % 16
            hexStr = chaDic.get(rest, str(rest)) + hexStr
            num //= 16
        hexStr = chaDic.get(num, str(num)) + hexStr
        return hexStr
    else:
        if num == -2147483648:  # 特殊情况，负数最大值
            return "80000000"
        num = -num  # 负数取反

        bitList = [0] * 31
        tail = 30
        while num >= 2:  # 数字转二进制
            rest = num % 2
            bitList[tail] = rest
            tail -= 1
            num //= 2
        bitList[tail] = num

        for i in range(31):  # 反码
            bitList[i] = 1 if bitList[i] == 0 else 0

        tail = 30
        add = 1
        while add + bitList[tail] == 2:  # 反码加１
            bitList[tail] = 0
            tail -= 1
        bitList[tail] = 1

        bitList = [1] + bitList  # 添加负号


        hexStr = ""
        for i in range(0, 32, 4):  # 二进制转16进制
            add = 0
            for j in range(0, 4):
                add += bitList[i + j] * 2 ** (3 - j)
            hexStr += chaDic.get(add, str(add))


for i in range(1, 0xFF+1):
    print(hex(i))