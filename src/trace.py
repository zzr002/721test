import subprocess
import os


def pre_exec():
    os.setpgid(0, 0)

class Trace():

    def start_trace(self):
        p = subprocess.Popen(["./../bin/diag", "./../trace/log"], bufsize=1, preexec_fn=pre_exec,
                                    shell=False, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = p.stdout.read()
        print(out)


#example
# tr = Trace()
# tr.start_trace()

