import logging
from datetime import datetime
from os import getpid
import sys


class Logger(object):
    def __init__(self, filename, flag=True, name=''):
        self.logger = logging.getLogger(str(getpid()) + name)
        self.logger.setLevel(logging.INFO)
        if flag:
            stdout = logging.StreamHandler(sys.stdout)
            stdout.setLevel(logging.INFO)
            self.logger.addHandler(stdout)
        self.handler = logging.FileHandler(filename)
        self.handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(message)s')
        self.handler.setFormatter(formatter)
        self.logger.addHandler(self.handler)

    def write(self, message):
        if message == '\n':
            return
        msg = '{}: {}'.format(datetime.now().strftime(
            '%Y-%m-%d %H:%M:%S'), message)
        self.logger.info(msg)

    def flush(self):
        pass

import serial
ser = serial.Serial(port="/dev/ttyUSB4",baudrate=115200)

while True:
    s = ser.read().decode()
    print(s)