import subprocess


class Auto():

    def __init__(self, com):
        self.com = com

    def reco_com(self):

        port = subprocess.Popen(["./bin/dev_name", "%s" % self.com], bufsize=1, shell=False, stdout=subprocess.PIPE)
        port_file = port.stdout.readline(100).decode().strip()
        return port_file

#
# a = Auto("at")
# a.reco_com()

