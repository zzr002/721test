# 721test

### 当前版本
V 0.4

### 介绍
自动AT命令工具

### 依赖模块
**请使用python3.6以上版本解释器**\
**sudo pip install pyserial**\
**sudo pip install matplotlib**\
**尽量输入一下这个命令吧：ulimit -n 2048**

### 使用方法
+ 使用本脚本需要安装上述依赖模块
+ 此脚本在linux系统下使用
+ 本模块当前有4种功能，接下来会一一展开
+ USB虚拟出的AT，DIAG，PPP已全部自动识别
#### **1.自动发送AT模块**
+ 本功能通过cd /721test
+ 执行sudo python3 manage.py执行
+ 当前trace存放在721test/trace下
+ 当前log存放在721test/log下

>tips：如果想要增加AT命令，只需在721test/cfg/base目录下增加即可
cmd.py下增加基础AT，init_cmd下增加只需执行一次的AT命令，timeout_cmd
增加需要时延较长的AT命令
注：init_cmd与timeout_cmd中的AT需要出现在cmd.py中

#### **2.USB ppp拨号一直ping，发现挂断自动重连**
+ 本功能通过cd /721test/PPP_CALL
+ 执行sudo python3 ppp_main.py
+ 当前trace存放在/721test/PPP_CALL/ppp_trace
+ 当前log存放在/721test/PPP_CALL/ppp_log
+ 当前分析图（ping有效值分析图）存放在/721test/PPP_CALL/ppp_diagiam
+ 具体log/trace/diagiam 名称为开启ppp_main的时间

#### **3.cmux下 ppp拨号测试。拨通ppp后，一直ping。发现挂断自动重连**
+ 当前模块没有自动识别功能，请手动更改/721test/PPP_CALL/cmux_main.py文件中ct = Cmux_Test("/dev/ttyUSB0")正确UART端口
+ 本功能通过cd /721test/PPP_CALL
+ 执行sudo python3 cmux_main.py
+ 当前trace存放在/721test/PPP_CALL/ppp_trace
+ 当前log存放在/721test/PPP_CALL/ppp_log
+ 当前分析图（ping有效值分析图）存放在/721test/PPP_CALL/ppp_diagiam
+ 具体log/trace/diagiam 名称为开启cmux_main的时间

#### **4.rndis不挂断，一直ping测试**
+ 本功能通过cd /721test/rndis
+ 执行sudo python3 rndis_main.py
+ 当前trace存放在/721test/rndis_trace
+ 当前log存放在/721test/rndis_log
+ 具体log/trace/diagiam 名称为开启cmux_main的时间

#### **5.tcp_send功能**
+ 本功能通过cd /721test/TCP_match
+ 执行sudo python3 main.py