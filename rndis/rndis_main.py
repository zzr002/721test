import os
import sys
print("请关闭其他网络，防止影响测试结果！！！")
pwd = os.getcwd()
f_pwd = os.path.dirname(pwd)
sys.path.insert(0, f_pwd)

import multiprocessing
from common.auto import Auto
from common.trace import Gene_Trace
from rndis.rndis_test import RnDis

diag = Auto("diag").reco_com()
gt = Gene_Trace(diag, "rndis_trace/")
p1 = multiprocessing.Process(target=gt.start_trace)
p1.start()

r = RnDis()

try:
    r.start_ping()
except Exception as e:
    raise e
finally:
    gt.stop_trace()